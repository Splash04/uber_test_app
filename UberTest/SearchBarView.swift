//
//  SearchBarView.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit

class SearchBarView: UIView {
    let rectSize: CGFloat = 6
    let lineOffset: CGFloat = 12
    let lineWidth: CGFloat = 1
    let iconSize: CGFloat = 34
    let iconOffset: CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let rectX = (bounds.height - rectSize)/2.0
        let rectY = (bounds.height - rectSize)/2.0
        let rectanglePath = UIBezierPath(rect: CGRect(x: rectX, y: rectY, width: rectSize, height: rectSize))
        let rectLayer = CAShapeLayer()
        
        let lineHeight = bounds.height - (lineOffset * 2)
        let linePath = UIBezierPath(rect: CGRect(x: bounds.width - (iconSize + iconOffset + iconOffset), y: lineOffset, width: lineWidth, height: lineHeight))
        
        rectanglePath.append(linePath)
        rectLayer.path = rectanglePath.cgPath
        rectLayer.fillColor = UIColor.black.cgColor
        
        layer.addSublayer(rectLayer)
    }
}
