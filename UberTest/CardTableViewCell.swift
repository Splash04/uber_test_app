//
//  CardTableViewCell.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit
import Kingfisher

class CardTableViewCell: UITableViewCell {
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    //@IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var posterImageView: UIImageView?
    
    var request: URLRequest?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.layer.cornerRadius = 7.0
        cardView.layer.masksToBounds = true
       // webView.scrollView.bounces = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//       webView.stopLoading()
//        request = nil
        posterImageView = nil
    }

    public func setup(withData: UberDataItem?) {
         mainTitleLabel.text = withData?.newsTitle
        
//        if let urlString = withData?.urlPath, let url = URL(string:urlString) {
//            request = URLRequest(url: url)
//        }
        
//        if request != nil {
//            webView.loadRequest(request!)
//        }
        
        if let imagePath = withData?.posterImagePath, let imageURL = URL(string: imagePath) {
            posterImageView?.kf.setImage(with: imageURL,
                                        placeholder: nil,
                                        options: [.transition(ImageTransition.fade(1))])
        }
    }
}
