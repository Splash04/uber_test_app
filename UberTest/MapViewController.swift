//
//  MapViewController.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController, GMSMapViewDelegate {
    let zoomLevel: Float = 14.0
    let defaultAnimationDuration: TimeInterval = 0.3
    var defaultLocationButtonWidth: CGFloat!
    var defaultLocationButtonHeight: CGFloat!
    var defaultXLocationButton: CGFloat!
    var defaultYLocationButton: CGFloat!
    var hiddenXLocationButton: CGFloat!
    var hiddenYLocationButton: CGFloat!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var myLocationButton: UIButton!
    
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 50
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {
        self.title = "Map"
        
        //Map
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        mapView.isHidden = true
        mapView.delegate = self
        calculateConstForAnimation()
    }
    
    func calculateConstForAnimation() {
        defaultLocationButtonWidth = self.myLocationButton.bounds.width
        defaultLocationButtonHeight = self.myLocationButton.bounds.height
        defaultXLocationButton = self.myLocationButton.frame.origin.x
        defaultYLocationButton = self.myLocationButton.frame.origin.y
        hiddenXLocationButton = self.myLocationButton.frame.origin.x + (defaultLocationButtonWidth / 2.0)
        hiddenYLocationButton = self.myLocationButton.frame.origin.y + (defaultLocationButtonHeight / 2.0)
    }
    
    @IBAction func onMyLocationClicked(_ sender: UIButton) {
        if let coordinateObject = mapView?.myLocation?.coordinate {
            mapView.animate(toLocation: coordinateObject)
            self.hideMyLocationButton(animated: true)
        }
    }
    
    func hideMyLocationButton(animated: Bool = false) {
        if(animated) {
            UIView.animate(withDuration: defaultAnimationDuration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] _ in // auto scrolling animation
                self?.myLocationButton.transform = ((self?.myLocationButton.transform)?.scaledBy(x: 0.01, y: 0.01))!
                }, completion: { [weak self] _ in
                    self?.myLocationButton.isHidden = true
            })
        } else {
            self.myLocationButton.isHidden = true
        }
    }
    
    func showMyLocationButton(animated: Bool = false) {
        self.myLocationButton.isHidden = false
        if(animated) {
            myLocationButton.isHidden = false
            UIView.animate(withDuration: defaultAnimationDuration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] _ in // auto scrolling animation
                self?.myLocationButton.transform = ((self?.myLocationButton.transform)?.scaledBy(x: 100.0, y: 100.0))!
                }, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if(gesture && myLocationButton.isHidden) {
            self.showMyLocationButton(animated: true)
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if (mapView.isHidden) {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            mapView?.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            self.mapView?.isMyLocationEnabled = true
            print("Location status is OK.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
