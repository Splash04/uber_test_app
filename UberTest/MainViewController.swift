//
//  ViewController.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit
import GoogleMaps

class MainViewController: UIViewController, CAAnimationDelegate {
    public enum CardViewType {
        case top, full, small
    }
    
    public var dataModel = UberDataModel()
    
    let bottomViewSmallSize: CGFloat = 70
    let topViewY: CGFloat = 80
    let fullViewY: CGFloat = 150
    let defaultAnimationDuration: TimeInterval = 0.8
    var smallViewY: CGFloat {
        return UIScreen.main.bounds.height - bottomViewSmallSize
    }
    let smallViewOffset: CGFloat = 16
    var smallViewWidth: CGFloat {
        return UIScreen.main.bounds.width - (smallViewOffset * 2)
    }
    var cardViewHeight: CGFloat!
    var widthCoeff: CGFloat!
    var titleTopX: CGFloat!
    var titleDefaultX: CGFloat!
    var titleTopY: CGFloat!
    var titleDefaultY: CGFloat!
    var titleTopXcoeff: CGFloat!
    var titleTopYcoeff: CGFloat!
    var topHeightOffset: CGFloat!
    var currentRorateCoef: CGFloat!
    var backButtonRodateIndex: Double!
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var viewOverlay: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBarView: SearchBarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(MainViewController.panGesture))
        let tap = UITapGestureRecognizer(target: self, action: #selector(MainViewController.tapGesture))
        gesture.delegate = self
        tableView.addGestureRecognizer(gesture)
        tableView.addGestureRecognizer(tap)
        tableView.contentInset = UIEdgeInsetsMake(0,0,tableView.frame.height + 5,0)
        calculateConstForAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        hideCards()
    }
    
    func calculateConstForAnimation() {
        cardViewHeight = smallViewY - fullViewY
        widthCoeff = smallViewOffset / cardViewHeight
        titleDefaultX = titleLabel.frame.minX
        titleDefaultY = titleLabel.frame.minY
        titleTopX = backButton.frame.minX * 2 + backButton.frame.width + 35
        titleTopY = backButton.frame.minY + 15
        let titleHorizontalOffset = titleTopX - titleDefaultX
        let titleVerticalOffset = titleDefaultY - titleTopY
        topHeightOffset = fullViewY - topViewY
        titleTopXcoeff = titleHorizontalOffset / topHeightOffset
        titleTopYcoeff = titleVerticalOffset / topHeightOffset
        currentRorateCoef = 0
        backButtonRodateIndex = -Double.pi / 2
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.view.layer.mask = nil
    }
    
    func setTopViewsAlpha(coefAlpha: CGFloat) {
        if (coefAlpha <= 0) {
            self.backButton.isHidden = true
            self.titleLabel.isHidden = true
            self.viewOverlay.isUserInteractionEnabled = false
        } else {
            self.backButton.alpha = coefAlpha
            self.titleLabel.alpha = coefAlpha
            self.backButton.isHidden = false
            self.titleLabel.isHidden = false
            self.viewOverlay.isUserInteractionEnabled = true
        }
    }
    
    func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        if tableView.contentOffset.y != 0 { //wait for top cell in table view
            return
        }
        
        let y = tableView.frame.minY
        let newRecognizerY = y + translation.y
        if (newRecognizerY >= fullViewY) && (newRecognizerY <= smallViewY) { // if scrolling between mid and bottom
            let newY = y + translation.y
            let currentHeight = newY - fullViewY
            let newX = widthCoeff * currentHeight
            let coefAlpha = (1 - (currentHeight / cardViewHeight)) * 2 // to hide view twice faster
            
            let overlayAlpha = coefAlpha > 1 ? 1: coefAlpha
            self.viewOverlay.alpha = overlayAlpha
            self.searchBarView.alpha = 1 - overlayAlpha
            setTopViewsAlpha(coefAlpha: coefAlpha - 1)
            tableView.frame = CGRect(x: newX, y: newY, width: view.frame.width - (newX * 2), height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if (newRecognizerY >= topViewY) && (newRecognizerY <= fullViewY) { // if scrolling between mid and top
            let newY = y + translation.y
            tableView.frame = CGRect(x: 0, y: newY, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
            let currentHeight = self.topHeightOffset - (newY - topViewY)
            titleLabel.frame = CGRect(x: titleDefaultX + (titleTopXcoeff * currentHeight),
                                      y: titleDefaultY - (titleTopYcoeff * currentHeight),
                                      width: titleLabel.frame.width,
                                      height: titleLabel.frame.height)
            
            let newRotateCoeff = (newRecognizerY - fullViewY) / topHeightOffset
            rotateBackButton(toCoeff: Double(newRotateCoeff))
        }
        
        if recognizer.state == .ended { // if end of scrolling
            var duration = velocity.y < 0 ? Double((y - fullViewY) / -velocity.y) : Double((smallViewY - y) / velocity.y ) // speed of auto scrolling between mid and bottom
            duration = duration > 1.3 ? 1 : duration
            if(newRecognizerY < fullViewY) {
                duration = velocity.y < 0 ? Double((y - topViewY) / -velocity.y) : Double((smallViewY - y) / velocity.y ) // speed of auto scrolling between mid and top
                duration = duration > 0.8 ? 0.5 : duration
            }
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] _ in // auto scrolling animation
                if let selfFrame = self?.view.frame {
                    var cardType: CardViewType  = .small
                    
                    if let smallY = self?.smallViewY, let fullY = self?.fullViewY, let topY = self?.topViewY {  //finding next view state (bottom, mid, top)
                        if (velocity.y >= 0) {
                            if (newRecognizerY < fullY && newRecognizerY > topY) {
                                cardType = .full
                            }
                        } else {
                            if (newRecognizerY > fullY) {
                                cardType = .full
                            } else {
                                cardType = .top
                            }
                        }
                        
                        switch cardType {   //setup views according state
                        case .small:
                            if let smallViewWidth = self?.smallViewWidth, let smallViewOffset = self?.smallViewOffset, let titleX = self?.titleDefaultX, let titleY = self?.titleDefaultY {
                                self?.viewOverlay.alpha = 0
                                self?.searchBarView.alpha = 1
                                self?.setTopViewsAlpha(coefAlpha: 0)
                                self?.tableView.frame = CGRect(x: smallViewOffset, y: smallY, width: smallViewWidth, height: selfFrame.height)
                                if let label = self?.titleLabel {
                                    label.frame = CGRect(x: titleX, y: titleY, width: label.frame.width, height: label.frame.height)
                                }
                            }
                        case .full:
                            if let titleX = self?.titleDefaultX, let titleY = self?.titleDefaultY {
                                self?.viewOverlay.alpha = 1
                                self?.searchBarView.alpha = 0
                                self?.setTopViewsAlpha(coefAlpha: 1)
                                self?.tableView.frame = CGRect(x: 0, y: fullY, width: selfFrame.width, height: selfFrame.height)
                                if let label = self?.titleLabel {
                                    label.frame = CGRect(x: titleX, y: titleY, width: label.frame.width, height: label.frame.height)
                                }
                                self?.rotateBackButton(toCoeff: 0.0, duration: duration)
                            }
                        case .top:
                            if let topTitleX = self?.titleTopX, let topTitleY = self?.titleTopY {
                                self?.viewOverlay.alpha = 1
                                self?.setTopViewsAlpha(coefAlpha: 1)
                                self?.tableView.frame = CGRect(x: 0, y: topY, width: selfFrame.width, height: selfFrame.height)
                                if let label = self?.titleLabel {
                                    label.frame = CGRect(x: topTitleX, y: topTitleY, width: label.frame.width, height: label.frame.height)
                                }
                                
                                self?.rotateBackButton(toCoeff: -1.0, duration: duration)
                            }
                        }
                    }
                }
                }, completion: { [weak self] _ in
                    if ( velocity.y < 0 ) {
                        self?.tableView.isScrollEnabled = true
                    }
            })
        }
    }
    
    func tapGesture(sender: UITapGestureRecognizer? = nil) {
        if self.tableView.frame.origin.y == self.smallViewY {
            UIView.animate(withDuration: defaultAnimationDuration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] _ in // auto scrolling animation
                if let selfFrame = self?.view.frame {
                    if let fullY = self?.fullViewY, let animationDuration = self?.defaultAnimationDuration {  //finding next view state (bottom, mid, top)
                        if let titleX = self?.titleDefaultX, let titleY = self?.titleDefaultY {
                            self?.viewOverlay.alpha = 1
                            self?.searchBarView.alpha = 0
                            self?.setTopViewsAlpha(coefAlpha: 1)
                            self?.tableView.frame = CGRect(x: 0, y: fullY, width: selfFrame.width, height: selfFrame.height)
                            if let label = self?.titleLabel {
                                label.frame = CGRect(x: titleX, y: titleY, width: label.frame.width, height: label.frame.height)
                            }
                            self?.rotateBackButton(toCoeff: 0.0, duration: animationDuration)
                        }
                    }
                }
                }, completion: nil)
        }
    }
    
    func rotateBackButton(toCoeff: Double, duration: TimeInterval = 0) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = currentRorateCoef
        currentRorateCoef = CGFloat(backButtonRodateIndex * toCoeff)
        rotateAnimation.toValue = currentRorateCoef
        rotateAnimation.fillMode = kCAFillModeForwards
        rotateAnimation.duration = duration
        rotateAnimation.isRemovedOnCompletion = false
        backButton.layer.add(rotateAnimation, forKey: nil)
    }
    
    func hideCards() {
        UIView.animate(withDuration: defaultAnimationDuration, delay: 0.0, options: [.allowUserInteraction], animations: { [weak self] _ in // auto scrolling animation
            if let selfFrame = self?.view.frame {
                if let smallViewWidth = self?.smallViewWidth, let smallViewOffset = self?.smallViewOffset, let titleX = self?.titleDefaultX, let titleY = self?.titleDefaultY, let smallY = self?.smallViewY {
                    self?.viewOverlay.alpha = 0
                    self?.setTopViewsAlpha(coefAlpha: 0)
                    self?.tableView.frame = CGRect(x: smallViewOffset, y: smallY, width: smallViewWidth, height: selfFrame.height)
                    if let label = self?.titleLabel {
                        label.frame = CGRect(x: titleX, y: titleY, width: label.frame.width, height: label.frame.height)
                    }
                }
            }
            }, completion: { [weak self] _ in
                self?.tableView.isScrollEnabled = false
        })
    }
}

extension MainViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = tableView.frame.minY
        var result = true
        if (y == topViewY && tableView.contentOffset.y == 0 && direction > 0) || (y > topViewY) {
            tableView.isScrollEnabled = false
            result = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return result
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataModel.getNumberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "CardCellIdentifier")!
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cardCell = cell as? CardTableViewCell {
            cardCell.setup(withData: dataModel.getItem(index: indexPath.row))
        }
    }
}
