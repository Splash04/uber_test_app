//
//  LocationButton.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit

class LocationButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = self.bounds.size.width / 2.0
        layer.masksToBounds = true
    }
}
