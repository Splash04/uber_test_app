//
//  UberDataModel.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import UIKit

class UberDataModel {
    var dataList = [UberDataItem]()
    init() {
        dataList.append(UberDataItem(title: "Request a Ride for a Loved One",
                                     url: "https://newsroom.uber.com/lovedone/",
                                     poster: "https://newsroom.uber.com/wp-content/uploads/2017/06/UberIM_002728-small.jpg"))
        dataList.append(UberDataItem(title: "Transforming transportation in Toronto",
                                     url: "https://newsroom.uber.com/atg-toronto/",
                                     poster: "https://newsroom.uber.com/wp-content/uploads/2017/05/Raquel-final-small-4.jpeg"))
        dataList.append(UberDataItem(title: "A Street Smart POOL Experience for Manhattan",
                                     url: "https://newsroom.uber.com/manhattanpool/",
                                     poster: "https://newsroom.uber.com/wp-content/uploads/2017/05/UberIM_001804-small-e1495465600930.jpg"))
        dataList.append(UberDataItem(title: "Что такое UBERSelect",
                                     url: "https://uberx.by/uber-select/",
                                     poster: "https://uberx.by/wp-content/uploads/2016/08/Russia_UberSELECT_Blog-960x480_r1-1-768x384.png"))
        dataList.append(UberDataItem(title: "UBER Снижает величину промокода",
                                     url: "https://uberx.by/uber-snizhaet-velichinu-promokoda/",
                                     poster: "https://uberx.by/wp-content/uploads/2017/03/GW_C30_03.jpg"))
        dataList.append(UberDataItem(title: "UBER с Анной Бонд!",
                                     url: "https://uberx.by/nachnite-uik-end-s-annoj-bond/",
                                     poster: "https://uberx.by/wp-content/uploads/2016/05/uberBOND-blog-1-768x432.png"))
        dataList.append(UberDataItem(title: "Движение “Зробiм!” и UBER. ",
                                     url: "https://uberx.by/dvizhenie-zrobim-i-uber-sdelaem-nash-gorod-luchshe/",
                                     poster: "https://uberx.by/wp-content/uploads/2016/04/content_uborka.jpg"))
    }
    
    public func getItem(index: Int) -> UberDataItem {
        return dataList[index]
    }
    
    public func getNumberOfItems() -> Int {
        return dataList.count
    }
}
