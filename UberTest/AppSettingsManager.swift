//
//  AppSettingsManager.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

import GoogleMaps

class AppSettingsManager: Any {
    public struct APIKeys {
        static let googleMapsToken = "AIzaSyB3WuUepgFJBfJvOZeDk8jkZvaqflqZay0"
    }
    
    static let sharedInstance = AppSettingsManager()
    
    init() {
    }
    
    public static func configure() {
        GMSServices.provideAPIKey(APIKeys.googleMapsToken)
    }
}
