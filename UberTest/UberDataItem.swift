//
//  UberDataItem.swift
//  UberTest
//
//  Created by mac-227 on 7/4/17.
//  Copyright © 2017 Test. All rights reserved.
//

class UberDataItem {
    var newsTitle: String?
    var urlPath: String?
    var posterImagePath: String?
    
    init(title: String? = nil, url: String? = nil, poster: String? = nil) {
        self.newsTitle = title
        self.urlPath = url
        self.posterImagePath = poster
    }
}
